stages:
  - check
  - test
  - upload
  - release

variables:
  MANIFEST_PATH: "build-aux/app.drey.Warp.Devel.json"
  FLATPAK_MODULE: "warp"
  RUNTIME: "master"
  WINDOWS_RUNTIME: "nightly"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/release/"

flatpak:
  image: "quay.io/gnome_infrastructure/gnome-runtime-images:gnome-$RUNTIME"
  tags:
    - flatpak
  variables:
    BUNDLE: "warp-nightly.flatpak"
    APP_ID: "app.drey.Warp.Devel"
    RUNTIME_REPO: "https://nightly.gnome.org/gnome-nightly.flatpakrepo"
  script:
    #    - flatpak install --user --noninteractive --verbose org.freedesktop.appstream.cli
    - >
      xvfb-run -a -s "-screen 0 1024x768x24"
      flatpak-builder --keep-build-dirs --user --disable-rofiles-fuse flatpak_app --repo=repo ${BRANCH:+--default-branch=$BRANCH} ${MANIFEST_PATH}
    #    - flatpak run --user org.freedesktop.appstream.cli validate --no-color --no-net flatpak_app/files/share/appdata/${APP_ID}.appdata.xml
    - flatpak build-bundle repo ${BUNDLE} --runtime-repo=${RUNTIME_REPO} ${APP_ID} ${BRANCH}
  artifacts:
    name: "Flatpak artifacts"
    expose_as: "Get Flatpak bundle here"
    when: "always"
    paths:
      - "${BUNDLE}"
    expire_in: 14 days

# Configure and run rustfmt
# Exits and builds fails if on bad format
rustfmt:
  image: "rust:slim"
  script:
    - rustup component add rustfmt
    - rustc -Vv && cargo -Vv
    - cargo fmt --version
    - cargo fmt --all -- --color=always --check

cargo-clippy:
  allow_failure: true
  extends: flatpak
  tags:
    - flatpak
  script:
    - flatpak-builder --user --disable-rofiles-fuse --keep-build-dirs --stop-at=${FLATPAK_MODULE} flatpak_app ${MANIFEST_PATH}
    - flatpak build-finish --share=network flatpak_app
    - echo "cargo clippy -- -D warnings" | flatpak-builder --user --disable-rofiles-fuse --build-shell=${FLATPAK_MODULE} flatpak_app ${MANIFEST_PATH}

cargo-deny:
  interruptible: true
  allow_failure: true
  image: rust
  script:
    - cargo install cargo-deny
    - cargo deny check

pages:
  image: quay.io/gnome_infrastructure/gnome-runtime-images:gnome-$RUNTIME
  tags:
    - flatpak
  interruptible: true
  script:
    - flatpak-builder --user --disable-rofiles-fuse --keep-build-dirs --stop-at=${FLATPAK_MODULE} flatpak_app ${MANIFEST_PATH}
    - flatpak build-finish --socket=x11 --share=network flatpak_app
    - echo "../build-aux/build-help-html.bash" | flatpak-builder --user --disable-rofiles-fuse --build-shell=${FLATPAK_MODULE} flatpak_app ${MANIFEST_PATH}
    - mkdir public
    - cp -r .flatpak-builder/build/${FLATPAK_MODULE}/_flatpak_build/help-out public/help
    - cp help/LINGUAS public/help/
    - chmod -R a=rwx public
  artifacts:
    paths:
      - "public"
#  rules:
#    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

windows:
  image: "ghcr.io/felinira/gtk4-cross-win:gnome-$WINDOWS_RUNTIME"
  stage: test
  allow_failure: true
  variables:
    BUNDLE: "warp-nightly-windows"
  script:
    # Build
    - |
      . ~/.cargo/env
      export PKG_CONFIG_PATH=/usr/lib64/pkgconfig:/usr/share/pkgconfig:/usr/x86_64-w64-mingw32/sys-root/mingw/lib/pkgconfig/:/usr/x86_64-w64-mingw32/lib/pkgconfig/
      # export CARGO_BUILD_TARGET="x86_64-pc-windows-gnu"

      mkdir -p package
      meson setup build -Dprefix="$PWD/package" -Dtarget="x86_64-pc-windows-gnu"
      ninja -C "build"
      ninja -C "build" install

    # Package
    - |
      package --no-strip

      # Application icon
      rsvg-convert ./data/icons/app.drey.Warp.svg | convert -scale 256x256 - ./data/icons/app.drey.Warp.ico
      wget https://github.com/electron/rcedit/releases/download/v1.1.1/rcedit-x64.exe
      wine rcedit-x64.exe "package/warp.exe" --set-icon "./data/icons/app.drey.Warp.ico"

      # Compile help to HTML
      for lang in package/share/help/*/warp; do
        yelp-build html $lang -o $lang
      done

      # Add gtk config
      mkdir -p package/etc/gtk-4.0
      printf "[Settings]\ngtk-decoration-layout=menu:close" > package/etc/gtk-4.0/settings.ini

      # Cleanup: delete empty folders
      find package -type d -empty -delete

      mv package "${BUNDLE}"
  artifacts:
    name: "Windows artifacts"
    expose_as: "Get Windows bundle here"
    paths:
      - "${BUNDLE}"
    expire_in: 14 days

upload:
  stage: upload
  image: "quay.io/gnome_infrastructure/gnome-runtime-images:gnome-$RUNTIME"
  needs:
    - job: windows
  rules:
    - if: $CI_COMMIT_TAG
  variables:
    BUNDLE: "warp-nightly-windows"
    FILE: "warp-windows-${CI_COMMIT_TAG}"
  script:
    - |
      mv ${BUNDLE} ${FILE}
      zip -r ${FILE}.zip ${FILE}/.
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${FILE}.zip "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${FILE}.zip"

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG # Run this job when a tag is created
  variables:
    WINDOWS_FILE: "warp-windows-${CI_COMMIT_TAG}.zip"
  script:
    - echo "running release_job"
  release: # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: "$CI_COMMIT_TAG"
    description: "${CI_COMMIT_TAG_MESSAGE%-----BEGIN PGP SIGNATURE-----*}"
    assets:
      links:
        - name: "${WINDOWS_FILE}"
          url: "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${WINDOWS_FILE}"
