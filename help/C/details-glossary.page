<?xml version="1.0" encoding="utf-8"?>
<!--
  DO NOT EDIT THIS FILE!

  This file is autogenerated with ducktype.
  Edit duck/details-glossary.page.duck instead and run make in this directory.
-->
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="details-glossary">
 <info>
  <link type="guide" xref="index#details"/>
  <desc>Definition of terms used through this help page</desc>
 </info>
 <title>Glossary</title>
 <section id="rendezvous-server">
  <title>Rendezvous Server</title>
  <p>This server is located on the internet and is operated by the
  <link href="https://github.com/magic-wormhole/magic-wormhole">Magic Wormhole project</link>. It is used to find peers to share
  files with. When starting a transmission, both parties will contact the rendezvous server to find each other.</p>
  <p>The rendezvous server also helps the transmitting client to generate a <link xref="#transmit-code"/></p>
  <p>They will use this server to exchange cryptographic information for secure communication. Afterwards they will exchange
  information about how to contact each other for file transfer.</p>
  <note>
   <p>No files are transferred via the rendezvous server. See <link xref="#transmit-relay"/> and <link xref="#direct-transfer"/></p>
  </note>
 </section>
 <section id="transmit-code">
  <title>Transmit Code</title>
  <p>This code will be generated by the client in cooperation with the <link xref="#rendezvous-server"/>. It is used to identify
  and encrypt the transmission of messages and files.</p>
  <p>To start a transmission you need a transmit code. The transmit code then needs to be communicated to the
  receiver (preferably via an encrypted or otherwise secure channel). After entering the transmit code at the receiver
  side the file transmission can begin.</p>
  <note>
   <p>Every transmit code can only be entered once for security reasons.</p>
  </note>
 </section>
 <section id="transmit-link">
  <title>Transmit Link</title>
  <p>It is also possible to encode the <link xref="#transmit-code"/> as a link. Clicking the link will automatically open Warp and
  start the file transmission. A <gui>Copy Transmit Link</gui> button is available in the send screen. A transmit link looks
  like this:</p>
  <code>wormhole-transfer:{code}</code>
 </section>
 <section id="transmit-relay">
  <title>Transmit Relay</title>
  <p>The transmit relay is also operated by the
  <link href="https://github.com/magic-wormhole/magic-wormhole">Magic Wormhole project</link></p>
  <p>It is used if no <link xref="#direct-transfer">direct communication</link> between two peers can be established. This is most
  commonly a problem when both parties are located in different home networks with
  <link href="https://en.wikipedia.org/wiki/Network_address_translation">NAT</link> or restrictive
  <link href="https://en.wikipedia.org/wiki/Firewall_(computing)">firewalls</link>.</p>
  <p>The files are transmitted via the transmit relay in an encrypted fashion. The relay will only know about the file size.</p>
  <p>Transfers via the transmit relay may be slower than direct transfer, depending on relay congestion.</p>
 </section>
 <section id="direct-transfer">
  <title>Direct Transfer</title>
  <p>If both peers can find a direct networking path between each other they will send the file directly. This is often
  referred to as a <link href="https://en.wikipedia.org/wiki/Peer-to-peer">Peer-to-Peer</link> connection. This type of
  connection is typically faster than a connection via the <link xref="#transmit-relay"/></p>
 </section>
</page>
